hi<?php
include_once 'DBConnection.php';
header('Content-Type: application/json');
	
	class MessageFriend {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function messageToFriend($user_one, $user_two, $messages, $gcm_token) {

			// $json['success'] = "Hello My Friend!";
			// echo json_encode($json);

			$hash_value = rand();

			try {

				$query = "SELECT hashID FROM message_group WHERE (user_one = '$user_one' AND user_two = '$user_two') OR (user_one = '$user_two' AND user_two = '$user_one');";
				$result = mysqli_query($this->connection, $query);


				//date and time to insert in messages table
				//date_default_timezone_set('America/Chicago');
				//$date = date('M-d-Y h:i:s a', time());

				if (mysqli_num_rows($result) == 1) {

					while ($hash_value = mysqli_fetch_array($result)) {
						$h_value = $hash_value['hashID'];
					}


					 //$query = "INSERT INTO `messages`(`id`, `hashID`, `sender_name`, `message`, `createdDate`, `recipient_gcm_token`) VALUES ('', '$h_value', '$user_one', '$messages', '$date', '$gcm_token');";
					 $query = "INSERT INTO `messages`(`id`, `hashID`, `sender_name`, `message`, `recipient_gcm_token`) VALUES ('', '$h_value', '$user_one', '$messages', '$gcm_token');";
					 $result = mysqli_query($this->connection, $query);

					 ////
					 	$pos = strpos($messages, " ", 20);
					 	$limit_message = substr($messages, 0, $pos)."...";

					 	$url = 'https://android.googleapis.com/gcm/send';

					 	if (strlen($messages) >= 20) {
					 		$fields = array(
								'registration_ids' => array($gcm_token),
								'data' => array("messages" => $limit_message,
												"senderName" => $user_one), //added senderName in the array
							);
					 	} else {
					 		$fields = array(
								'registration_ids' => array($gcm_token),
								'data' => array("messages" => $messages,
												"senderName" => $user_one), //added senderName in the array
							);
					 	}

						$headers = array(
								'Authorization: key='.'AIzaSyDYeXUyq5rQSmek4Ei957LhI3fCYBg4Nqc',
								'Content-Type: application/json'
							);

						//open connection
						$ch = curl_init();

						//Set the url, number of POST vars, POST data
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

						//execute post
						$result = curl_exec($ch);

						//close the connection
						curl_close($ch);
						echo $result;

					 ////


					 // if ($result == 1) {
					 	$query = "SELECT sender_name, message FROM messages WHERE hashID = '$h_value';";
					 	$select_result = mysqli_query($this->connection, $query);

					 	if(mysqli_num_rows($select_result) > 0) {

					 		$json = array();

						 	while ($row = mysqli_fetch_assoc($select_result)) {
						 		
						 		array_push($json, $row);

						 	}

						 	echo json_encode($json);
						 	//$json['success'] = $sender_name_value + " message is " + $msg_value;
						 	

					 	}

				} else {

					$query = "INSERT INTO message_group (user_one, user_two, hashID, user_two_regTokenID) VALUES ('$user_one', '$user_two', '$hash_value', '$gcm_token');";
					$msg_query = "INSERT INTO `messages`(`id`, `hashID`, `sender_name`, `message`, `recipient_gcm_token`) VALUES ('', '$hash_value', '$user_one', '$messages', '$gcm_token');";
					
					$result = mysqli_query($this->connection, $query);

					if ($result == 1) {

						$json['success'] = 'Start the conversation now!';
						echo json_encode($json);

					}

					mysqli_query($this->connection, $msg_query);

				}
				
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			
		} 
		
	}

	$msgFriend = new MessageFriend();
	if(isset($_POST['user_one_name'], $_POST['user_two_name'], $_POST['messages'], $_POST['gcm_token'])) {

		$user_one = $_POST['user_one_name'];
		$user_two = $_POST['user_two_name'];
		$messages = $_POST['messages'];
		$gcm_token = $_POST['gcm_token'];
		
		if(!empty($user_one) && !empty($user_two) && !empty($messages) && !empty($gcm_token)){
			
			$msgFriend-> messageToFriend($user_one, $user_two, $messages, $gcm_token);
			
		} else {
			$json['error'] = "Message didn't go through!";
			echo json_encode($json);
		}
		
	}

?>