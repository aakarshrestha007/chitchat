<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class AddImagePath {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function addUserImagePath($username) {
	
		try {
			$query = "SELECT id FROM friend_list WHERE (user_one = '$username' OR user_two = '$username');";
			$result = mysqli_query($this->connection, $query);

			if (mysqli_num_rows($result) >= 1) {
				
				//getting the userone image path and storing the value in $uone_image_path string
				$query_to_get_userone_image_path = "SELECT image_path FROM users WHERE username = '$username';";
				$result_of_userone_image_path = mysqli_query($this->connection, $query_to_get_userone_image_path);

				if (mysqli_num_rows($result_of_userone_image_path) == 1) {
					while ($userone_image_path = mysqli_fetch_array($result_of_userone_image_path)) {
						$uone_image_path = $userone_image_path['image_path'];
					}
				}

				//getting the userone image path and storing the value in $utwo_image_path string
				$query_to_get_usertwo_image_path = "SELECT image_path FROM users WHERE username = '$username';";
				$result_of_usertwo_image_path = mysqli_query($this->connection, $query_to_get_usertwo_image_path);

				if (mysqli_num_rows($result_of_usertwo_image_path) == 1) {
					while ($usertwo_image_path = mysqli_fetch_array($result_of_usertwo_image_path)) {
						$utwo_image_path = $usertwo_image_path['image_path'];
					}
				}

				//getting the userone username and storing the value in $uone_username string
				$query_to_get_userone_username = "SELECT user_one FROM friend_list WHERE user_one = '$username';";
				$result_of_userone_username = mysqli_query($this->connection, $query_to_get_userone_username);

				if (mysqli_num_rows($result_of_userone_username) >= 1) {

					$json_one = array();
					while ($userone_username = mysqli_fetch_array($result_of_userone_username)) {
						$uone_username = $userone_username['user_one'];
						array_push($json_one, $uone_username);
					}

					$uone_uname = json_encode(($json_one[0]));
					
				}

				$update_query_one = "UPDATE friend_list SET user_one_image_path = '$uone_image_path' WHERE user_one = '$username';";
				$result_update_one = mysqli_query($this->connection, $update_query_one);

				$update_query_two = "UPDATE friend_list SET user_two_image_path = '$utwo_image_path' WHERE user_two = '$username';";
				$result_update_two = mysqli_query($this->connection, $update_query_two);

				if ($result_update_one == 1 || $result_update_two == 1) {
					$json['success'] = "Successfully added the image path!";
				} else {
					$json['error'] = "Problem adding the image path!";
				}

				echo json_encode($json);
				mysqli_close($this->connection);

			}

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	

}

$addImagePath = new AddImagePath();

if (isset($_POST['username'])) {
	
	$username = $_POST['username'];

	if (!empty($username)) {
		$addImagePath->addUserImagePath($username);	
	}
}