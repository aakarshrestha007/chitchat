<?php
include_once 'DBConnection.php';
header('Content-Type: application/json');
	
	class MessageList {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function messageFromFriend($user_one, $user_two) {

			// $json['success'] = "Hello My Friend!";
			// echo json_encode($json);

			try {
				
				$query = "SELECT hashID FROM message_group WHERE (user_one = '$user_one' AND user_two = '$user_two') OR (user_one = '$user_two' AND user_two = '$user_one');";
				$result = mysqli_query($this->connection, $query);

				if (mysqli_num_rows($result) == 1) {
					while ($hash_value = mysqli_fetch_array($result)) {
						$h_value = $hash_value['hashID'];
					}

					$query = "SELECT sender_name, message, createdDate FROM messages WHERE hashID = '$h_value';";
			 		$select_result = mysqli_query($this->connection, $query);

			 		if(mysqli_num_rows($select_result) > 0) {
			 			$json = array();

					 	while ($row = mysqli_fetch_assoc($select_result)) {
					 		
					 		array_push($json, $row);

					 	}

					 	echo json_encode($json);
			 		}


				}

			} catch (Exception $e) {
				throw new Exception($e->getMessage());
				
			}

		} 	
				
		
		
	}

	$msgList = new MessageList();
	if(isset($_GET['user_one_name'], $_GET['user_two_name'])) {

		$user_one = $_GET['user_one_name'];
		$user_two = $_GET['user_two_name'];
		
		if(!empty($user_one) && !empty($user_two)){
			
			$msgList-> messageFromFriend($user_one, $user_two);
			
		} else {
			$json['error'] = "No message is there for you!";
			echo json_encode($json);
		}
		
	}









?>