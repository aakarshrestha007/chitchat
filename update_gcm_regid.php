<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
	class UpdateGcmRegID {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function updateUserGcmRegID($username) {

			$queryUser = "SELECT username FROM users WHERE username = '$username';";
			$result = mysqli_query($this->connection, $queryUser);
			if (mysqli_num_rows($result) > 0) {

				$query = "UPDATE users SET gcm_regid = 'logged out' WHERE username = '$username';";
				$result = mysqli_query($this->connection, $query);

				if ($result == 1) {
					$json['success'] = "Account updated!";
				} else {
					$json['error'] = "Error updating the account!";
				}

			} else {
				$json['error'] = "Not found!";
			}

			echo json_encode($json);
			//close the db connection
			mysqli_close($this->connection);
			
		} 
		
	}
	
	
	$updateGcmRegID = new UpdateGcmRegID();
	if(isset($_POST['username'])) {

		$username = $_POST['username'];
		
		if(!empty($username)){
			
			$updateGcmRegID-> updateUserGcmRegID($username);
			
		} else {
			$json['error'] = "Username is missing!";
			echo json_encode($json);
		}
		
	}









?>