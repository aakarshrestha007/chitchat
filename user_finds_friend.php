<?php
include_once 'DBConnection.php';
header('Content-Type: application/json');
	
	class UserFindsFriend {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function getAllUsers($username) {

			try {

				$query = "SELECT username, email, createdDate, gcm_regid FROM users WHERE username LIKE '$username%' ORDER BY username;";
				$result = mysqli_query($this->connection, $query);

				if (mysqli_num_rows($result)) {
					$json = array();
					while ($row = mysqli_fetch_assoc($result)) {
						array_push($json, $row);
					}

					echo json_encode($json);
				} else {
					$json_error['error'] = "Nothing matched!";
					echo json_encode($json_error);
				}
				
			} catch (Exception $e) {
				echo $e;
			}
			
		} 
		
	}
	
	
	$userFindsFriend = new UserFindsFriend();
	if(isset($_GET['username'], $_GET['token'])) {

		$username = $_GET['username'];
		$token = $_GET['token'];
		$token_string = "7kPbftbrcpAKs9K";
		
		if(!empty($username) && !empty($token)){
			
			if ($token == $token_string) {
				$userFindsFriend-> getAllUsers($username);
			} else {
				echo "Authentication Denied!";
			}
			
		} else {
			$json['error'] = "Username is missing!";
			echo json_encode($json);
		}
		
	}









?>