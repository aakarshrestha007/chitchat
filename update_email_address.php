<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
	class UpdateEmailAddress {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}

		public function updateEmail($username, $email_address, $new_email_address){
			
			$queryToEmail = "SELECT email FROM users WHERE username = '$username' AND email = '$email_address';";
			$result = mysqli_query($this->connection, $queryToEmail);
			if (mysqli_num_rows($result) == 1) {

				$queryToNewEmail = "SELECT email FROM users WHERE email = '$new_email_address';";
				$resultOfNewEmail = mysqli_query($this->connection, $queryToNewEmail);
				if (mysqli_num_rows($resultOfNewEmail) > 0) {
					$json['error'] = $new_email_address." already exists! Try a different email address.";
				} else {
					$query = "UPDATE users SET email = '$new_email_address' WHERE username = '$username' AND email = '$email_address';";
					$result = mysqli_query($this->connection, $query);

					if ($result == 1) {
						$json['success'] = "Email address updated!";
					} else {
						$json['error'] = "Error updating the email address!";
					}
				}					

			} else {
				$json['error'] = "Not found!";
			}

			echo json_encode($json);
			//close the db connection
			mysqli_close($this->connection);

		}
		
	}
	
	
	$updateEmailAddress = new UpdateEmailAddress();
	if(isset($_POST['username'], $_POST['email_address'], $_POST['new_email_address'])) {

		$username = $_POST['username'];
		$email_address = $_POST['email_address'];
		$new_email_address = $_POST['new_email_address'];
		
		if(!empty($username) && !empty($email_address) && !empty($new_email_address)){
			
			$updateEmailAddress-> updateEmail($username, $email_address, $new_email_address);
			
		} else {
			$json['error'] = "Username or Email address is missing!";
			echo json_encode($json);
		}
		
	}









?>