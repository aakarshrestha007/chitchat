<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
	class User {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function getAllUsers($username) {

			$query = "SELECT username, email, createdDate, image_path, gcm_regid FROM users WHERE username = '$username';";
			$result = mysqli_query($this->connection, $query);
			$json = array();
			while ($row = mysqli_fetch_assoc($result)) {
				array_push($json, $row);
			}

			echo json_encode($json);
			
		} 
		
	}
	
	
	$user = new User();
	if(isset($_GET['username'], $_GET['token'])) {

		$username = $_GET['username'];
		$token = $_GET['token'];
		$token_string = "7kPbftbrcpAKs9K";

		if(!empty($username) && !empty($token)){
			
			if ($token == $token_string) {
				$user-> getAllUsers($username);
			} else {
				echo "Authentication Denied!";
			}
			
		} else {
			$json['error'] = "Authentication Denied!";
			echo json_encode($json);
		}
		
	}









?>