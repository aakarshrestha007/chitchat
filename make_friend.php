<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class UserMakeFriend {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function makeFriend($username, $otheruser_username, $gcm_token) {

				$query = "SELECT id FROM friend_list WHERE (user_one = '$username' AND user_two = '$otheruser_username') OR (user_one = '$otheruser_username' AND user_two = '$username');";
				$result = mysqli_query($this->connection, $query);

				if(mysqli_num_rows($result) == 1) {
					$json['error'] = "You " . "(".$username.")" . " and " . $otheruser_username . " are friends already!";
					echo json_encode($json);
				} else {

					try {
						//getting the userone image path and storing the value in $uone_image_path string
						$query_to_get_userone_image_path = "SELECT image_path FROM users WHERE username = '$username';";
						$result_of_userone_image_path = mysqli_query($this->connection, $query_to_get_userone_image_path);
						
						if (mysqli_num_rows($result_of_userone_image_path) == 1) {
							while ($userone_image_path = mysqli_fetch_array($result_of_userone_image_path)) {
								$uone_image_path = $userone_image_path['image_path'];
							}	
						}

						//getting the usertwo image path and storing the value in $utwo_image_path string
						$query_to_get_usertwo_image_path = "SELECT image_path FROM users WHERE username = '$otheruser_username';";
						$result_of_usertwo_image_path = mysqli_query($this->connection, $query_to_get_usertwo_image_path);

						if (mysqli_num_rows($result_of_usertwo_image_path) == 1) {
							while ($usertwo_image_path = mysqli_fetch_array($result_of_usertwo_image_path)) {
								$utwo_image_path = $usertwo_image_path['image_path'];
							}
						}

						$query = "INSERT INTO friend_list (user_one, user_two, request_status, user_one_image_path, user_two_image_path, user_two_gcm_token) VALUES ('$username', '$otheruser_username', '1', '$uone_image_path', '$utwo_image_path', '$gcm_token');";
						$inserted = mysqli_query($this -> connection, $query);
						if($inserted == 1 ){
							$json['success'] = "Friend invitation sent!";

							/*
							Google Cloud Messaging
							*/

							$url = 'https://android.googleapis.com/gcm/send';
							$fields = array(
									'registration_ids' => array($gcm_token),
									'data' => array("sender_name" => $username), //added senderName in the array
								);

							$headers = array(
									'Authorization: key='.'AIzaSyDYeXUyq5rQSmek4Ei957LhI3fCYBg4Nqc',
									'Content-Type: application/json'
								);

							//open connection
							$ch = curl_init();

							//Set the url, number of POST vars, POST data
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

							//execute post
							$result = curl_exec($ch);

							//close the connection
							curl_close($ch);
							//echo $result;

						}else{
							$json['error'] = "Sorry, unable to send friend invitation!";
						}

						echo json_encode($json);
						mysqli_close($this->connection);
						
					} catch (Exception $e) {
						throw new Exception($e->getMessage());
						
					}

				}
	}

}

$usermfriend = new UserMakeFriend();

if (isset($_POST['username'], $_POST['otheruser_username'], $_POST['gcm_token'])) {
	
	$username = $_POST['username'];
	$otheruser_username = $_POST['otheruser_username'];
	$gcm_token = $_POST['gcm_token'];

	if (!empty($username) && !empty($otheruser_username) && !empty($gcm_token)) {
	
			$usermfriend->makeFriend($username, $otheruser_username, $gcm_token);

	} else {
		$json['error'] ='Sorry, something went wrong. Please try again!';
		echo json_encode($json);
	}

}








