<?php
include_once 'DBConnection.php';
header('Content-Type: application/json');
	
	class FriendshipTag {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function showFriendshipTag($username) {

			try {
					//$query = "SELECT u.username, u.image_path, u.image_name, fl.user_one, fl.user_two, fl.request_status FROM users u JOIN friend_list fl ON fl.user_two = u.username WHERE ('$username' = fl.user_one) AND (fl.user_one = '$username' OR fl.user_two = '$username') AND fl.request_status = '2';";
					
					$query = "SELECT user_one, user_two, user_one_image_path, user_two_image_path FROM friend_list WHERE request_status = '2' AND user_one = '$username';";
					$result = mysqli_query($this->connection, $query);

					if (mysqli_num_rows($result) > 0) {
						$json = array();
						while ($row = mysqli_fetch_assoc($result)) {
							array_push($json, $row);
						}
						echo json_encode($json);
					} else {
						$json['error'] = 'no friend!';
						echo json_encode($json);
					}

					mysqli_close($this->connection);
						
				} catch (Exception $e) {
					throw new Exception($e->getMessage());
					
			}
			
		} 
		
	}

	$user = new FriendshipTag();
	if(isset($_GET['username'], $_GET['token'])) {

		$username = $_GET['username'];
		$token = $_GET['token'];
		$token_string = "7kPbftbrcpAKs9K";

		if(!empty($username) && !empty($token)){
			
			if ($token == $token_string) {
				$user-> showFriendshipTag($username);
			} else {
				echo "Authentication Denied!";
			}
			
		} else {
			$json['error'] = "The field is missing value!";
			echo json_encode($json);
		}
		
	}









?>