<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class GetPostComment {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function showComments($friendname) {

		try {
				$query = "SELECT userone, comment, createdDate FROM post_comment WHERE usertwo = '$friendname' ORDER by createdDate DESC;";
				$result = mysqli_query($this->connection, $query);

				$json = array();
				while ($row = mysqli_fetch_assoc($result)) {
					array_push($json, $row);
				}

				echo json_encode($json);
				mysqli_close($this->connection);
						
			} catch (Exception $e) {
				throw new Exception($e->getMessage());	
			}

	}	


}

$getPostComment = new GetPostComment();

if (isset($_GET['friendname'], $_GET['token'])) {
	
	$friendname = $_GET['friendname'];
	$token = $_GET['token'];
	$token_string = "7kPbftbrcpAKs9K";

	if (!empty($friendname) && !empty($token)) {
			
			if ($token == $token_string) {
				$getPostComment->showComments($friendname);
			} else {
				echo "Authentication Denied!";
			}

	} else {
		$json['error'] ='Sorry, something went wrong. Please try again!';
		echo json_encode($json);
	}


}
