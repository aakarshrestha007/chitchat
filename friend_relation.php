<?php
include_once 'DBConnection.php';
header('Content-Type: application/json');
	
	class UserFriendRelation {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function showFriendRelation($username, $otheruser_username) {

			try {

					$query = "SELECT id FROM friend_list WHERE (user_one ='$username' AND user_two = '$otheruser_username') OR (user_one ='$otheruser_username' AND user_two = '$username');";
					$result = mysqli_query($this->connection, $query);

					$bool_confirmation = false;
					if (mysqli_num_rows($result) == 1) {
						$json = array();
						while ($row = mysqli_fetch_assoc($result)) {
						array_push($json, $row);
						$bool_confirmation['success'] = true;
						}
						//echo json_encode($json);
						echo json_encode($bool_confirmation);
					} else {
						$bool_confirmation['error'] = false;
						echo json_encode($bool_confirmation);
					}
						
				} catch (Exception $e) {
					throw new Exception($e->getMessage());
					
			}
			
		} 
		
	}

	$user = new UserFriendRelation();
	if(isset($_POST['username'], $_POST['otheruser_username'])) {

		$my_id = $_POST['username'];
		$otheruser_id = $_POST['otheruser_username'];
		
		if(!empty($my_id) && !empty($otheruser_id)){
			
			$user-> showFriendRelation($my_id, $otheruser_id);
			
		} else {
			$json['error'] = "One of the fields is missing value!";
			echo json_encode($json);
		}
		
	}









?>