<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class UserSignup {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function signup_user($username, $email, $password, $registrationToken) {

		$query = "SELECT * FROM users WHERE username = '$username' OR email = '$email';";
		$result = mysqli_query($this->connection, $query);
		if (mysqli_num_rows($result)>0) {
			$json['error'] = $username. " or ". $email . " or both already exist!";
			echo json_encode($json);
		} else {

			try {
					$query = "INSERT INTO users (username, email, password, gcm_regid) values ('$username','$email','$password', '$registrationToken');";
					$inserted = mysqli_query($this -> connection, $query);
					if($inserted == 1 ){
						$json['success'] = 'Account is created!!!';
					}else{
						$json['error'] = $username . " or " . $email ." already exists!";
					}
					echo json_encode($json);
					mysqli_close($this->connection);
					
				} catch (Exception $e) {
					throw new Exception($e->getMessage());
					
				}

		}

				
			
	}
}

$usersignup = new UserSignup();

if (isset($_POST['username'], $_POST['email'], $_POST['password'], $_POST['confirmpassword'], $_POST['registrationToken'])) {
	
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$confirmpassword = $_POST['confirmpassword'];
	$registrationToken = $_POST['registrationToken'];

	if (!empty($username) && !empty($email) && !empty($password) && !empty($confirmpassword) && !empty($registrationToken)) {
		if ($confirmpassword == $password) {
			$encrypt_password = md5($password);
			$usersignup->signup_user($username, $email, $encrypt_password, $registrationToken);
		} else {
			$json['error'] ='Password does not match!';
			echo json_encode($json);
		}
		
	} else {
		$json['error'] ='All fields are required!';
		echo json_encode($json);
	}


}
