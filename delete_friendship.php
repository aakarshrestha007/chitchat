<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class DeleteFriendShip {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function deleteFriend($username, $otheruser_username) {

		try {
				$query = "DELETE FROM friend_list WHERE (user_one = '$username' AND user_two = '$otheruser_username') OR (user_one = '$otheruser_username' AND user_two = '$username');";
				//"UPDATE friend_list SET request_status = '3' WHERE (user_one = '$username' AND user_two = '$otheruser_username') OR (user_one = '$otheruser_username' AND user_two = '$username');";
		
				$deleted = mysqli_query($this -> connection, $query);
				if($deleted == 1 ){
					$json['success'] = 'Friendship deleted!';
				}else{
					$json['error'] = "Sorry, unable to delete the friendship!";
				}

				echo json_encode($json);
				mysqli_close($this->connection);	

			} catch (Exception $e) {
				throw new Exception($e->getMessage());
				
			}	
		
	}
}

$userDeleteFriendShip = new DeleteFriendShip();

if (isset($_POST['username'], $_POST['otheruser_username'])) {
	
	$username = $_POST['username'];
	$otheruser_username = $_POST['otheruser_username'];

	if (!empty($username) && !empty($otheruser_username)) {
	
			$userDeleteFriendShip->deleteFriend($username, $otheruser_username);

	} else {
		$json['error'] ='Sorry, something went wrong. Please try again!';
		echo json_encode($json);
	}


}
