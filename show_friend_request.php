<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class ShowFriendRequest {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function showFriendRequestStatus($username) {

		try {
				//$query = "SELECT user_one, user_two, request_status FROM friend_list WHERE user_one = '$username' AND request_status = '1';";

				$query = "SELECT user_one, user_two, request_status FROM friend_list WHERE (user_one = '$username' OR user_two = '$username') AND request_status = '1';";
				$result = mysqli_query($this->connection, $query);

				$json = array();
				while ($row = mysqli_fetch_assoc($result)) {
					array_push($json, $row);
				}

				echo json_encode($json);
				mysqli_close($this->connection);
						
			} catch (Exception $e) {
				throw new Exception($e->getMessage());	
			}

	}	


}

$show_friend_request = new ShowFriendRequest();

if (isset($_GET['username'], $_GET['token'])) {
	
	$username = $_GET['username'];
	$token = $_GET['token'];
	$token_string = "7kPbftbrcpAKs9K";

	if (!empty($username) && !empty($token)) {
	
			if ($token == $token_string) {
				$show_friend_request->showFriendRequestStatus($username);
			} else {
				echo "Authentication Denied!";
			}

	} else {
		$json['error'] ='Sorry, something went wrong. Please try again!';
		echo json_encode($json);
	}


}
