<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
	class PostComment {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}
		
		
		public function postTheComment($username, $friendName, $comment, $gcm_token) {

			try {

				$query = "INSERT INTO post_comment (userone, usertwo, comment, user_two_gcm_token) VALUES ('$username', '$friendName', '$comment', '$gcm_token');";
				$result = mysqli_query($this -> connection, $query);

				if ($result == 1) {
					$json['success'] = "Post successfully!";

					/*
					Google Cloud Messaging
					*/

					$url = 'https://android.googleapis.com/gcm/send';
					$fields = array(
							'registration_ids' => array($gcm_token),
							'data' => array("commentor_name" => $username), //added senderName in the array
						);

					$headers = array(
							'Authorization: key='.'AIzaSyDYeXUyq5rQSmek4Ei957LhI3fCYBg4Nqc',
							'Content-Type: application/json'
						);

					//open connection
					$ch = curl_init();

					//Set the url, number of POST vars, POST data
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

					//execute post
					$result = curl_exec($ch);

					//close the connection
					curl_close($ch);
					//echo $result;

				} else{
					$json['error'] = "Oops! Please try again!";
				}
				echo json_encode($json);
				mysqli_close($this->connection);
				
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}
		
	}
	
	
	$postComment = new PostComment();
	if(isset($_POST['username'], $_POST['friendname'], $_POST['comment'], $_POST['gcm_token'])) {

		$username = $_POST['username'];
		$friendName = $_POST['friendname'];
		$comment = $_POST['comment'];
		$gcm_token = $_POST['gcm_token'];
		
		if(!empty($username) && !empty($friendName) && !empty($comment) && !empty($gcm_token)){
			
			$postComment-> postTheComment($username, $friendName, $comment, $gcm_token);
			
		} else {
			$json['error'] = "All fields are required!";
			echo json_encode($json);
		}
		
	}

?>