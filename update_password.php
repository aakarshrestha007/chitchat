<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
	class UpdatePassword {
		
		private $db;
		private $connection;
		
		function __construct() {
			$this -> db = new DB_Connection();
			$this -> connection = $this->db->getConnection();
		}

		public function updateUserPassword($username, $current_password, $encrypt_password) {

			$queryToUsername = "SELECT username FROM users WHERE username = '$username';";
			$result = mysqli_query($this->connection, $queryToUsername);
			if (mysqli_num_rows($result) == 1) {

				$queryToCurrentPassword = "SELECT password FROM users WHERE username = '$username';";
				$resultOfCurrentPassword = mysqli_query($this->connection, $queryToCurrentPassword);
				if (mysqli_num_rows($resultOfCurrentPassword) == 1) {
					while ($currentPass = mysqli_fetch_array($resultOfCurrentPassword)) {
						$current_pass_word = $currentPass['password'];
					}
				}

				$encrypt_current_password = md5($current_password);
				if ($encrypt_current_password == $current_pass_word) {
					
					$query = "UPDATE users SET password = '$encrypt_password' WHERE username = '$username';";
					$result = mysqli_query($this->connection, $query);

					if ($result == 1) {
						$json['success'] = "Password updated!";
					} else {
						$json['error'] = "Error updating the password!";
					}	

				} else {
					$json['error'] = "Current password does not match!";
				}
								

			} else {
				$json['error'] = "Not found!";
			}

			echo json_encode($json);
			//close the db connection
			mysqli_close($this->connection);

		}
		
	}
	
	
	$updatePassword = new UpdatePassword();
	if(isset($_POST['username'], $_POST['current_password'], $_POST['new_password'], $_POST['confirm_password'])) {

		$username = $_POST['username'];
		$current_password = $_POST['current_password'];
		$new_password = $_POST['new_password'];
		$confirm_password = $_POST['confirm_password'];
		
	if (!empty($username) && !empty($current_password) && !empty($new_password) && !empty($confirm_password)) {
		
		if ($confirm_password == $new_password) {
			$encrypt_password = md5($new_password);
			$updatePassword->updateUserPassword($username, $current_password, $encrypt_password);
		} else {
			$json['error'] ='Password does not match!';
			echo json_encode($json);
		}
		
	} else {
		$json['error'] ='All fields are required!';
		echo json_encode($json);
	}
		
}









?>