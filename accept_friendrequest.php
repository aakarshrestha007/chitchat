<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class AcceptFriendRequest {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function friendRequestAccepted($username, $otheruser_username) {

		$query = "SELECT id FROM friend_list WHERE (user_one = '$username' AND user_two = '$otheruser_username') OR (user_one = '$otheruser_username' AND user_two = '$username');";
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 1) {

			try {
				$query = "UPDATE friend_list SET request_status = '2' WHERE (user_one = '$username' AND user_two = '$otheruser_username') OR (user_one = '$otheruser_username' AND user_two = '$username');";
				$result = mysqli_query($this -> connection, $query);
				if($result == 1 ){
					$json['success'] = 'Congratulations! You guys are friends now!';
				}else{
					$json['error'] = "Sorry, unable to establish the friendship!";
				}
				echo json_encode($json);
				mysqli_close($this->connection);
				
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
				
			}

		} 
				
	}
}

$acceptFriendRequest = new AcceptFriendRequest();

if (isset($_POST['username'], $_POST['otheruser_username'])) {
	
	$username = $_POST['username'];
	$otheruser_username = $_POST['otheruser_username'];

	if (!empty($username) && !empty($otheruser_username)) {
	
			$acceptFriendRequest->friendRequestAccepted($username, $otheruser_username);

	} else {
		$json['error'] ='Sorry, something went wrong. Please try again!';
		echo json_encode($json);
	}


}
