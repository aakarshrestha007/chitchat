<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');
//header('Content-type : bitmap, chartset=uft-8');

class UserSignupCopy {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function signup_user($username, $email, $password, $encoded_string, $image_name, $registrationToken) {

		if (empty($encoded_string) && empty($image_name)) {
			
			$query = "SELECT * FROM users WHERE username = '$username' OR email = '$email';";
			$result = mysqli_query($this->connection, $query);
			if (mysqli_num_rows($result)>0) {
				$json['error'] = $username. " or ". $email . " already exists!";
				echo json_encode($json);
			} else {

					try {
						$decode_string = base64_decode($encoded_string);
						$path = "images/".$image_name;

						$query = "INSERT INTO users (username, email, password, image_name, image_path, gcm_regid) values ('$username','$email','$password', '$image_name', '$path', '$registrationToken');";
						$inserted = mysqli_query($this -> connection, $query);
						if($inserted == 1 ){
							$json['success'] = 'Account is created!!!';
						}else{
							$json['error'] = "Oops! Please try again.";
						}
						echo json_encode($json);
						mysqli_close($this->connection);
						
					} catch (Exception $e) {
							throw new Exception($e->getMessage());
							
					}

			}


		} else {
			$decode_string = base64_decode($encoded_string);
			$path = "images/".$image_name;

			$query = "SELECT * FROM users WHERE username = '$username' OR email = '$email';";
			$result = mysqli_query($this->connection, $query);
			if (mysqli_num_rows($result)>0) {
				$json['error'] = $username. " or ". $email . " already exists!";
				echo json_encode($json);
			} else {

				$file = fopen($path, 'wb');
				$is_written = fwrite($file, $decode_string);
				fclose($file);

				if ($is_written > 0) {
					
					try {

						$query = "INSERT INTO users (username, email, password, image_name, image_path, gcm_regid) values ('$username','$email','$password', '$image_name', '$path', '$registrationToken');";
						$inserted = mysqli_query($this -> connection, $query);
						if($inserted == 1 ){
							$json['success'] = 'Account is created!!!';
						}else{
							$json['error'] = "Oops! Please try again.";
						}
						echo json_encode($json);
						mysqli_close($this->connection);
						
					} catch (Exception $e) {
							throw new Exception($e->getMessage());
							
					}

				}

			}
		}
			
	}
}

$usersignupcopy = new UserSignupCopy();

if (isset($_POST['username'], $_POST['email'], $_POST['password'], $_POST['confirmpassword'], $_POST['registrationToken'])) {
	
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$confirmpassword = $_POST['confirmpassword'];
	$encoded_string = $_POST['encoded_string'];
	$image_name = $_POST['image_name'];
	$registrationToken = $_POST['registrationToken'];

	if (!empty($username) && !empty($email) && !empty($password) && !empty($confirmpassword) && !empty($registrationToken)) {
		if ($confirmpassword == $password) {
			$encrypt_password = md5($password);
			$usersignupcopy->signup_user($username, $email, $encrypt_password, $encoded_string, $image_name, $registrationToken);
		} else {
			$json['error'] ='Password does not match!';
			echo json_encode($json);
		}
		
	} else {
		$json['error'] ='All fields are required!';
		echo json_encode($json);
	}

}
