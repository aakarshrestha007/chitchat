<?php

include_once 'DBConnection.php';
header('Content-Type: application/json');

class UploadImage {

	private $db;
	private $connection;
	
	function __construct()
	{
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function upload_image_of_user($username, $encoded_string, $image_name) {

		if (empty($encoded_string) && empty($image_name)) {
			
			$query = "SELECT * FROM users WHERE username = '$username';";
			$result = mysqli_query($this->connection, $query);
			if (mysqli_num_rows($result)==1) {
				
				try {
					$decode_string = base64_decode($encoded_string);
					$path = "images/".$image_name;

					$query = "UPDATE users SET image_name = '$image_name', image_path = '$path' Where username = '$username';";
					$inserted = mysqli_query($this -> connection, $query);
					if($inserted == 1 ){
						$json['success'] = 'Photo uploaded successfully!!!';
					}else{
						$json['error'] = "Oops! Please try again.";
					}
					echo json_encode($json);
					mysqli_close($this->connection);
					
				} catch (Exception $e) {
						throw new Exception($e->getMessage());	
				}

			}

		} else {
			$decode_string = base64_decode($encoded_string);
			$path = "images/".$image_name;

			$query = "SELECT * FROM users WHERE username = '$username';";
			$result = mysqli_query($this->connection, $query);
			if (mysqli_num_rows($result)==1) {
			
				$file = fopen($path, 'wb');
				$is_written = fwrite($file, $decode_string);
				fclose($file);

				if ($is_written > 0) {
					
					try {

						$query = "UPDATE users SET image_name = '$image_name', image_path = '$path' Where username = '$username';";
						$inserted = mysqli_query($this -> connection, $query);
						if($inserted == 1 ){
							$json['success'] = 'Photo uploaded successfully!!!';
						}else{
							$json['error'] = "Oops! Please try again.";
						}
						echo json_encode($json);
						mysqli_close($this->connection);
						
					} catch (Exception $e) {
							throw new Exception($e->getMessage());
							
					}

				}

			}
		}
			
	}
}

$uploadImage = new UploadImage();

if (isset($_POST['username'])) {
	
	$username = $_POST['username'];
	$encoded_string = $_POST['encoded_string'];
	$image_name = $_POST['image_name'];

	if (!empty($username)) {
		$uploadImage->upload_image_of_user($username, $encoded_string, $image_name);
	} else {
		$json['error'] ='Username is missing!';
		echo json_encode($json);
	}

}
